package servlets;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet ("/hello")
public class HelloServlet extends HttpServlet {
		
		
		public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
			
			

			float loan = Integer.parseInt(request.getParameter("loan"));
			float instalmentAmount = Integer.parseInt(request.getParameter("instalmentAmount"));
			float bankRate = Float.parseFloat(request.getParameter("bankRate"));
			float fixedPrice = Float.parseFloat(request.getParameter("fixedPrice"));
			String type = request.getParameter("type");
			
			
			if(loan==0||instalmentAmount==0||bankRate==0||fixedPrice==0){
				response.sendRedirect("/");
			}
			
			
			if("stala".equals(type)){
				double q =  1+((bankRate/100)/12);
				float power_q =  (float) Math.pow(q, instalmentAmount);
				float instalment = (float) (loan*power_q*(q-1)/(power_q-1));
				float interest = (float) (loan* (bankRate/100)* (30.0/360.0));
				float capitalCase=instalment-interest;
				float wholeInstalment = interest + capitalCase+fixedPrice;
				for(int i = 1;i<=instalmentAmount;i++){
					loan -=capitalCase ;
				 interest = (float) (loan* (bankRate/100)* (30.0/360.0));
				 capitalCase=instalment-interest;
				  wholeInstalment = interest + capitalCase+fixedPrice;
				
			response.setContentType("text/html; charset=ISO-8859-2");
			response.getWriter().println(   "<table border ='1'><tr><td>Nr raty</td>	<td>Kwota Kapita�u</td><td>Kwota odsetek</td><td>Op�aty sta�e</td><td>Ca�kowita kwota raty</td></tr>");
			response.getWriter().println( 	"<tr><td>"+i+"</td>	<td>" +Math.floor(capitalCase*100)/100+"</td><td>" +Math.floor(interest*100)/100 +"</td><td>" +Math.floor(fixedPrice*100)/100+"</td><td>" +Math.floor(wholeInstalment*100)/100+"</td></tr>"); 
					}} else{
						float capitalPart=loan/instalmentAmount; 
						float interestPart=loan*bankRate/100/12;	
						for(int i = 1;i<=instalmentAmount;i++){
							loan = loan -capitalPart;
							interestPart=loan*bankRate/100/12;
							float wholeInstalment = interestPart + capitalPart+fixedPrice;
							response.setContentType("text/html; charset=ISO-8859-2");
							response.getWriter().println("<table border ='1'><tr><td>Nr raty</td>	<td>Kwota Kapita�u</td><td>Kwota odsetek</td><td>Op�aty sta�e</td><td>Ca�kowita kwota raty</td></tr>");
							response.getWriter().println( 	"<tr><td>"+i+"</td>	<td>" +Math.floor(capitalPart*100)/100+"</td><td>" +Math.floor(interestPart*100)/100+"</td><td>" +Math.floor(fixedPrice*100)/100+"</td><td>" +Math.floor(wholeInstalment*100)/100+"</td></tr>"); 
							
						}
						
						
					}
				response.getWriter().println( "</table> ");  
			}
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
