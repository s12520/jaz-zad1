
import static org.junit.Assert.*;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

import servlets.HelloServlet;

public class HelloServletTest extends Mockito {

	
	@Test
	public void servlet_should_not_show_table_if_the_inquiry_is_empty() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		HelloServlet servlet = new HelloServlet();
		
		when(request.getParameter("loan")).thenReturn("0");
		when(request.getParameter("instalmentAmount")).thenReturn("0");
		when(request.getParameter("bankRate")).thenReturn("0");
		when(request.getParameter("fixedPrice")).thenReturn("0");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	

	}
	
	@Test
	public void servlet_should_not_show_table_if_the_inquiry_has_1_opion_filled() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		HelloServlet servlet = new HelloServlet();
		
		when(request.getParameter("loan")).thenReturn("10000");
		when(request.getParameter("instalmentAmount")).thenReturn("0");
		when(request.getParameter("bankRate")).thenReturn("0");
		when(request.getParameter("fixedPrice")).thenReturn("0");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	

	}
	@Test
	public void servlet_should_not_show_table_if_the_inquiry_has_2_opions_filled() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		HelloServlet servlet = new HelloServlet();
		
		when(request.getParameter("loan")).thenReturn("10000");
		when(request.getParameter("instalmentAmount")).thenReturn("10");
		when(request.getParameter("bankRate")).thenReturn("0");
		when(request.getParameter("fixedPrice")).thenReturn("0");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	

	}
	@Test
	public void servlet_should_not_show_table_if_the_inquiry_has_3_opions_filled() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		HelloServlet servlet = new HelloServlet();
		
		when(request.getParameter("loan")).thenReturn("10000");
		when(request.getParameter("instalmentAmount")).thenReturn("10");
		when(request.getParameter("bankRate")).thenReturn("10");
		when(request.getParameter("fixedPrice")).thenReturn("0");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	

	}
	@Test
	public void servlet_should_not_show_table_if_the_inquiry_has_only_2nd_opion_filled() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		HelloServlet servlet = new HelloServlet();
		
		when(request.getParameter("loan")).thenReturn("0");
		when(request.getParameter("instalmentAmount")).thenReturn("10");
		when(request.getParameter("bankRate")).thenReturn("0");
		when(request.getParameter("fixedPrice")).thenReturn("0");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	

	}
	

}
